/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojchart'],
        function (oj, ko, $) {

            function ChartViewModel() {
                var self = this;

                self.series = ko.observableArray();
                self.groups = ko.observableArray();
                // Below are a set of the ViewModel methods invoked by the oj-module component.
                // Please reference the oj-module jsDoc for additional information.

                /**
                 * Optional ViewModel method invoked after the View is inserted into the
                 * document DOM.  The application can put logic that requires the DOM being
                 * attached here. 
                 * This method might be called multiple times - after the View is created 
                 * and inserted into the DOM and after the View is reconnected 
                 * after being disconnected.
                 */
                self.connected = function () {
                    $.getJSON("http://localhost:8080/microservice-example/trip/count-by-type")
                            .then((data) => {
                                const items = [];
                                const groups = [];
                                $.each(data, (i, item) => {
                                    items.push(item.count);
                                    groups.push(item.tripType);
                                });
                                self.series([
                                    {
                                        name: 'Viajes',
                                        items: items
                                    }
                                ]);
                                self.groups(groups);
                            });
                };

                /**
                 * Optional ViewModel method invoked after the View is disconnected from the DOM.
                 */
                self.disconnected = function () {

                };

                /**
                 * Optional ViewModel method invoked after transition to the new View is complete.
                 * That includes any possible animation between the old and the new View.
                 */
                self.transitionCompleted = function () {
                    var chartModel = new ChartModel();
                    ko.applyBindings(chartModel, document.getElementById('chart-container'));

                };

            }

            /*
             * Returns a constructor for the ViewModel so that the ViewModel is constructed
             * each time the view is displayed.  Return an instance of the ViewModel if
             * only one instance of the ViewModel is needed.
             */
            return new ChartViewModel();
        }
);
