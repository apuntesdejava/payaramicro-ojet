package com.apuntesdejava.microprofile.repository;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author diego
 */
@ApplicationScoped
public class JPAProducer {

    @Produces
    @PersistenceContext
   
    private EntityManager em;
}
