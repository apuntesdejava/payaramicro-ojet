package com.apuntesdejava.microprofile.repository;

import com.apuntesdejava.microprofile.response.CountTypeResponse;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 *
 * @author diego
 */
@ApplicationScoped
public class TripTypeRepository {

    @Inject
    private EntityManager em;

    public List<CountTypeResponse> findByTypes() {

        TypedQuery<CountTypeResponse> query = em.createNamedQuery("TripType.countByType", CountTypeResponse.class);
        return query.getResultList();
    }
}
