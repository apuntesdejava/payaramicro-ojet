package com.apuntesdejava.microprofile.repository;

import com.apuntesdejava.microprofile.domain.Person;
import java.util.Date;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

/**
 *
 * @author diego
 */
@ApplicationScoped
public class PersonRepository {

    @Inject
    private EntityManager em;

    public List<Person> findAll() {
        return em.createQuery("select c from Person c", Person.class)
                .getResultList();
    }

    public Person findById(Long id) {
        return em.find(Person.class, id);
    }

    @Transactional
    public Person edit(Person c) {
        return em.merge(c);
    }

    @Transactional
    public void delete(long customerId) {

        em.remove(em.find(Person.class, customerId));
    }

    @Transactional
    public Person create(String firstName, String lastName) {
        Person c = new Person();
        c.setName(firstName);
        c.setJobTitle(lastName);
        c.setFrequentFlyer(true);
        c.setLastUpdated(new Date());
        em.persist(c);
        return c;
    }
}
