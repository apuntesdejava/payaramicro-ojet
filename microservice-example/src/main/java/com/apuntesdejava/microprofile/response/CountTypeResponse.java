package com.apuntesdejava.microprofile.response;

/**
 *
 * @author diego
 */
public class CountTypeResponse {

    private String tripType;
    private Number count;

    public CountTypeResponse() {
    }

    public CountTypeResponse(String tripType, Number count) {
        this.tripType = tripType;
        this.count = count;
    }

    public CountTypeResponse(Object[] row) {
        for (Object field : row) {
            if (field instanceof String) {
                tripType = (String) field;
            } else if (field instanceof Number) {
                count = (Number) field;
            }
        }
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public Number getCount() {
        return count;
    }

    public void setCount(Number count) {
        this.count = count;
    }

}
