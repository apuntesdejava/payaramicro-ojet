package com.apuntesdejava.microprofile.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author diego
 */
@Entity
@Table(name = "triptype")
@NamedQueries({
    @NamedQuery(name = "TripType.countByType", query = "select new com.apuntesdejava.microprofile.response.CountTypeResponse( t.tripType.name, count(t) ) from Trip t group by t.tripType")
})
public class TripType implements Serializable {

    private static final long serialVersionUID = 1891637827425631166L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "tripTypeId")
    private long tripTypeId;

    private String name;

    private String description;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date lastUpdated;

    public long getTripTypeId() {
        return tripTypeId;
    }

    public void setTripTypeId(long tripTypeId) {
        this.tripTypeId = tripTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

}
