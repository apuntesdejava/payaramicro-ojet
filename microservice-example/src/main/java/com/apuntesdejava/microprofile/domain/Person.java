package com.apuntesdejava.microprofile.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author diego
 */
@Entity
@Table(name = "person")
public class Person implements Serializable {

    private static final long serialVersionUID = 5691958663385494922L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "personid")
    private long personId;

    @Column(name = "name")
    private String name;

    @Column(name = "jobtitle")
    private String jobTitle;

    @Column(name = "lastUpdated")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date lastUpdated;

    @Column(name = "frequentflyer")
    private boolean frequentFlyer;

    public long getPersonId() {
        return personId;
    }

    public void setPersonId(long personId) {
        this.personId = personId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public boolean isFrequentFlyer() {
        return frequentFlyer;
    }

    public void setFrequentFlyer(boolean frequentFlyer) {
        this.frequentFlyer = frequentFlyer;
    }

}
