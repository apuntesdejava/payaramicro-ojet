package com.apuntesdejava.microprofile.endpoint;

import com.apuntesdejava.microprofile.repository.TripTypeRepository;
import com.apuntesdejava.microprofile.response.CountTypeResponse;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author diego
 */
@Path("triptype")
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class TripTypeEndpoint {

    @Inject
    private TripTypeRepository repository;

    @GET
    public Response findCount() {
        List<CountTypeResponse> list = repository.findByTypes();
        return Response.ok(list).build();
    }
}
