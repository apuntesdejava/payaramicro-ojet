package com.apuntesdejava.microprofile.endpoint;

import com.apuntesdejava.microprofile.domain.Person;
import com.apuntesdejava.microprofile.repository.PersonRepository;
import java.util.Date;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author diego
 */
@Path("person")
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class PersonEndpoint {

    @Inject
    private PersonRepository personRepository;

    @GET
    public Response findAll() {
        List<Person> list = personRepository.findAll();
        return Response.ok(list).build();
    }

    @GET
    @Path("{customerId}")
    public Response findById(@PathParam("customerId") long customerId) {
        Person customer = personRepository.findById(customerId);
        if (customer == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(customer).build();
    }

    @POST
    public Response create(
            @FormParam(value = "name") String name,
            @FormParam(value = "jobtitle") String jobTitle) {
        Person customer = personRepository.create(name, jobTitle);
        return Response.ok(customer).build();
    }

    @PUT
    @Path("{personId}")
    public Response update(
            @PathParam("personId") long personId,
            @FormParam("name") String name,
            @FormParam("jobtitle") String jobtitle
    ) {
        Person customer = personRepository.findById(personId);
        if (customer == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        customer.setName(name);
        customer.setJobTitle(jobtitle);
        customer.setLastUpdated(new Date());
        return Response.ok(personRepository.edit(customer)).build();
    }

    @DELETE
    @Path("{customerId}")
    public Response delete(@PathParam("customerId") long customerId) {
        Person customer = personRepository.findById(customerId);
        if (customer == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        personRepository.delete(customerId);
        return Response.ok().build();
    }

}
